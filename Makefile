app: funzioni.o main.o
	g++ -o app funzioni.o main.o

funzioni.o: funzioni.cpp funzioni.hpp
	g++ -c funzioni.cpp

main.o: main.cpp funzioni.hpp
	g++ -c main.cpp